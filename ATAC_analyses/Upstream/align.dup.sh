# general directory where all the processing is located
dir=""
# get names of the files with extension (1 file name per cell)
# i had cram files located in the untar folder 
variable=$(ls $dir/untar/ )
# should look like this f.e.
# variable="scATAC-seq8125199.cram"

for x in $variable
do
	# get the basename of the file without extension
        basenamefile=$(basename "$dir/untar/$x" .cram)
	# use bwa to align 2 fastq files (r1 and r2) to the hg38 genome 
        bwa mem -t 8 hg38.fa $dir/fastq/$basenamefile.r1.fastq.gz $dir/fastq/$basenamefile.r2.fastq.gz | samtools view -bS - | samtools sort > $dir/bam2/$basenamefile.bam
	# get the stats for the initial reads per cell
        samtools flagstat $dir/bam2/$basenamefile.bam > $dir/log2/$basenamefile.initial.log
	# mark duplicates 
        java -Xmx30g -jar $EBROOTPICARD/picard.jar MarkDuplicates INPUT=$dir/bam2/$basenamefile.bam OUTPUT=$dir/tmp/$basenamefile.markdup.bam ASSUME_SORTED=true METRICS_FILE=$dir/log2/$basenamefile.metrics.log VALIDATION_STRINGENCY=LENIENT REMOVE_DUPLICATES=false 
	# remove duplicates and bad quality reads 
        samtools view -@ 8 -F 1804 -f 2 -b $dir/tmp/$basenamefile.markdup.bam  > $dir/bam2/$basenamefile.clean.bam
	# stats after removing duplicates
        samtools flagstat $dir/bam2/$basenamefile.clean.bam > $dir/log2/$basenamefile.rmdup.log
        # indexing bam file
        samtools index $dir/bam2/$basenamefile.clean.bam
	# removing unnecessary intermediate files 
        rm $dir/bam2/$basenamefile.bam
        rm $dir/tmp/$basenamefile.markdup.bam

done 

