import scanpy as sc
sc.settings.verbosity = 0

import numpy as np
import pandas as pd

from matplotlib import pylab as plt
import seaborn as sns

from IPython.display import display
import plotly as py
import plotly.graph_objs as go
from plotly.subplots import make_subplots
py.offline.init_notebook_mode(connected=True)

import umap

class scRNA_functions:
    def __init__(self):
        
        print(" * Initialising ...")
        self.__annotationMatrix = ''

        
        
# ****************************************** Private functions ******************************************
    # Finding the ribosomial genes in the provided annotation matrix
    def __findRibosomialGenes(self, verbose=True):
    
        rps_genes = self.__annotationMatrix.id[self.__annotationMatrix.symbol.str.startswith('RPS')]
        rpl_genes = self.__annotationMatrix.id[self.__annotationMatrix.symbol.str.startswith('RPL')]

        genes = rps_genes.append(rpl_genes)

        if verbose:
            for rib in genes:
                print(rib, end=" ")
            print("\n")

        return genes
        

    # Findind the mitochondrial genes in the provided annotation matrix
    def __findMitochondrialGenes(self, verbose=True):
    
        genes = self.__annotationMatrix.id[self.__annotationMatrix.symbol.str.startswith('MT-')]

        if verbose:
            for mt in genes:
                print(mt, end=" ")
            print("\n")

            for mt in self.__annotationMatrix.symbol[self.__annotationMatrix.symbol.str.match('MT-')]:
                print(mt, end=" ")
            print("\n")

            print("\n")

        return genes
    
    
    # Coverting the palette into hexadecimal format
    def __rgb2hex(self, palette):

        colors = []

        if isinstance(palette[0][0], float): 
            for i in range(len(palette)):
                r = int(255*palette[i][0])
                g = int(255*palette[i][1])
                b = int(255*palette[i][2])
                colors.append('#%02x%02x%02x' % (r,g,b))
        else:
            for i in range(len(palette)):
                r = int(palette[i][0])
                g = int(palette[i][1])
                b = int(palette[i][2])
                colors.append('#%02x%02x%02x' % (r,g,b))

        return colors
    

    # Defining and use a simple function to label the plot in axes coordinates
    def __label(self, x, color, label):
        ax = plt.gca()
        ax.text(-.2, .2, label, fontweight="bold",
                ha="left", va="center", transform=ax.transAxes)


    # Plotting a single ridge plot
    def __plotSingleRidgePlot(self, df, palette, height=0.75):
        
        sns.set(style="white", rc={"axes.facecolor": (0, 0, 0, 0)})
        
        g = sns.FacetGrid(df, row=df.columns[1], hue=df.columns[1], aspect=15, height=height, palette=palette)

        # Draw the densities in a few steps
        g.map(sns.kdeplot, df.columns[0], clip_on=False, shade=True, alpha=1, lw=1.5, bw=.2)
        g.map(sns.kdeplot, df.columns[0], clip_on=False, color="w", lw=2, bw=.2)
        g.map(plt.axhline, y=0, lw=2, clip_on=False)

        g.map(self.__label, df.columns[0])

        # Set the subplots to overlap
        g.fig.subplots_adjust(hspace=-.25)

        # Remove axes details that don't play well with overlap
        g.set_titles("")
        g.set(yticks=[])
        g.set_xlabels(df.columns[0], fontweight="bold")
        g.despine(bottom=True, left=True)



# ****************************************** Loading functions ******************************************
    # Reading the provided annotation matrix
    def readAnnotationMatrix(self, path, delimiter='\t', verbose=True):
        
        if verbose:
            print(" * Reading Annotation Matrix, path=%s\n"%path)
    
        self.__annotationMatrix = pd.read_csv(path, delimiter=delimiter)
        
    
    # Reading the excel file with the marker genes
    def readMarkerListFoetal(self, markerListPath, marker_genes, key, variable="id", sheet_name=None):
    
        print(" * Reading the given marker list")
        
        df    = pd.read_excel(markerListPath, sheet_name=sheet_name)
        genes = df[variable].tolist()
        
        print(" * Adding %s marker genes to %s"%(sheet_name, key))
        marker_genes[key] = genes
        
    
    # Loading the gene expression matrix provided by Sanger pipeline and building the scanpy object
    def loadSmartSeqSanger(self, path, delimiter='\t', mit=True, ercc=True, rib=True, verbose=True,
                           min_genes=0, min_cells=0, title=""):
        
        if verbose:
            print(" * Loading sample, path=%s"%path)

        rawData = pd.read_csv(path, delimiter=delimiter)
        rawData = rawData.iloc[0:-4:,:]
        indeces = rawData.iloc[:,0]

        names   = {}
        for i,idx in enumerate(indeces):
            names[i] = idx

        cols = {}
        for i,col in enumerate(rawData.columns):
            cols[col] = 'X' + col

        rawData.rename(index=names, inplace=True)
        rawData       = rawData.drop(columns=['Unnamed: 0'])
        rawData.rename(columns=cols, inplace=True)
        countsRawData = rawData[rawData.index.str.match('ENSG0')]
        
        smartSeq = sc.AnnData(countsRawData.T)

        if min_genes > 0:
            sc.pp.filter_cells(smartSeq, min_genes=min_genes)

        if min_cells > 0:
            sc.pp.filter_genes(smartSeq, min_cells=min_cells)

        smartSeq.obs['sample'] = title
        smartSeq.obs['sample'] = smartSeq.obs['sample'].astype('category')
        
        smartSeq.obs['n_counts']   = smartSeq.X.sum(1)
        smartSeq.obs['log_counts'] = np.log(smartSeq.obs['n_counts'])
        smartSeq.obs['n_genes']    = (smartSeq.X > 0).sum(1)
        
        if mit:
            try:
                if verbose:
                    print(" * Calculating the mitochondrial percentage")
                    
                mitGenes = self.__findMitochondrialGenes(verbose=False)
                smartSeq.obs['percent_mito'] = np.sum(rawData.loc[mitGenes].T, axis=1) / smartSeq.obs['n_counts']
                
            except:
                print(" * Warning: the Annotation Matrix must be provided to calculate the mitochondrial percentage!")

        if ercc:
            erccGenes = rawData.index[rawData.index.str.startswith('ERCC-')] 
            smartSeq.obs['ercc_content'] = np.sum(rawData.loc[erccGenes].T, axis=1) / np.sum(rawData.T, axis=1)
            
            if verbose:
                print(" * Calculating the ercc content")

        if rib:
            try:
                if verbose:
                    print(" * Calculating the ribosomial percentage")
                    
                ribGenes = self.__findRibosomialGenes(verbose=False)
                smartSeq.obs['percent_rib'] = np.sum(rawData.loc[ribGenes].T, axis=1) / smartSeq.obs['n_counts']
                
            except:
                print(" * Warning: the Annotation Matrix must be provided to calculate the ribosomial percentage!")
        
        if verbose:
            print(" * Initial SmartSeq2 Object: %d genes across %d single cells"%(smartSeq.n_vars, smartSeq.n_obs))
            print("\n")

        return smartSeq
    

    # Loading the gene expression matrix provided by CRUCK pipeline and building the scanpy object
    def loadSmartSeqCRUK(self, path, delimiter='\t', mit=True, ercc=True, rib=True, verbose=True,
                         min_genes=0, min_cells=0, title=""):
        
        if verbose:
            print(" * Loading sample, path=%s"%path)

        rawData = pd.read_csv(path, delimiter=delimiter)
        rawData = rawData.iloc[0:-4:,:]
        rawData = rawData[rawData.columns.drop(list(rawData.filter(regex='lostreads')))]
        indeces = rawData.iloc[:,0]

        names   = {}
        for i,idx in enumerate(indeces):
            names[i] = idx

        cols = {}
        for i,col in enumerate(rawData.columns):
            split    = col.split(".")[0:2]
            if len(split) == 1:
                cols[col] = col
            else:
                split[1]  = split[1].replace("_", "-")
                cols[col] = split[0]+split[1]

        rawData.rename(index=names, inplace=True)
        rawData       = rawData.drop(columns=['Unnamed: 0'])

        rawData.rename(columns=cols, inplace=True)
        countsRawData = rawData[rawData.index.str.match('ENSG0')]
        
        smartSeq = sc.AnnData(countsRawData.T)

        if min_genes > 0:
            sc.pp.filter_cells(smartSeq, min_genes=min_genes)

        if min_cells > 0:
            sc.pp.filter_genes(smartSeq, min_cells=min_cells)

        smartSeq.obs['sample'] = title
        smartSeq.obs['sample'] = smartSeq.obs['sample'].astype('category')
        
        smartSeq.obs['n_counts']   = smartSeq.X.sum(1)
        smartSeq.obs['log_counts'] = np.log(smartSeq.obs['n_counts'])
        smartSeq.obs['n_genes']    = (smartSeq.X > 0).sum(1)
        
        if mit:
            try:
                if verbose:
                    print(" * Calculating the mitochondrial percentage")
                    
                mitGenes = self.__findMitochondrialGenes(verbose=False)
                smartSeq.obs['percent_mito'] = np.sum(rawData.loc[mitGenes].T, axis=1) / smartSeq.obs['n_counts']
                
            except:
                print(" * Warning: the Annotation Matrix must be provided to calculate the mitochondrial percentage!")

        if ercc:
            erccGenes = rawData.index[rawData.index.str.startswith('ERCC-')] 
            smartSeq.obs['ercc_content'] = np.sum(rawData.loc[erccGenes].T, axis=1) / np.sum(rawData.T, axis=1)
            
            if verbose:
                print(" * Calculating the ercc content")

        if rib:
            try:
                if verbose:
                    print(" * Calculating the ribosomial percentage")
                    
                ribGenes = self.__findRibosomialGenes(verbose=False)
                smartSeq.obs['percent_rib'] = np.sum(rawData.loc[ribGenes].T, axis=1) / smartSeq.obs['n_counts']
                
            except:
                print(" * Warning: the Annotation Matrix must be provided to calculate the ribosomial percentage!")
        
        if verbose:
            print(" * Initial SmartSeq2 Object: %d genes across %d single cells"%(smartSeq.n_vars, smartSeq.n_obs))
            print("\n")

        return smartSeq
    


# ****************************************** Computation functions ******************************************
    # Getting the annotation matrix as dataframe
    def getAnnotationMatrix(self):
        return self.__annotationMatrix

    
    # Mapping Ensembl IDs to the corresponding gene names
    def mappingEnsemblToAnnotated(self, adata, verbose=True, raw_data=True):
    
        if verbose:
            print(" * Applying the mapping based on the provided Annotation Matrix ...")

        IDs = list(set(self.__annotationMatrix.id).intersection(set(adata.var.index.tolist())))
        adata.var["Ensembl"] = adata.var.index.tolist()

        filtered = self.__annotationMatrix[self.__annotationMatrix.id.isin(IDs)]
        names    = pd.Series(filtered.symbol.values, index=filtered.id).to_dict()

        adata.var.rename(index=names, inplace=True)
        
        if verbose:
            print(" * Gene names renamed from ensembl to annotated gene name")
        
        if raw_data:
            try:
                adata.raw.var.rename(index=names, inplace=True)

                if verbose:
                    print(" * Gene names renamed (raw data) from ensembl to annotated gene name")

            except:
                pass


    # Mapping gene names to the corresponding Ensembl IDs
    def mappingAnnotatedToEnsembl(self, adata, verbose=True, raw_data=True):
    
        if verbose:
            print(" * Applying the mapping based on the provided Annotation Matrix ...")
        
        IDs = list(set(self.__annotationMatrix.symbol).intersection(set(adata.var.index.tolist())))

        filtered = self.__annotationMatrix[self.__annotationMatrix.symbol.isin(IDs)]
        names    = pd.Series(filtered.id.values, index=filtered.symbol).to_dict()
        
        if verbose:
            print(" * Gene names renamed from annotated to ensembl gene name")
            
        if raw_data:
            try:
                
                adata.raw.var.rename(index=names, inplace=True)

                if verbose:
                    print(" * Gene names renamed (raw data) from ensembl to annotated gene name")

            except:
                pass


    # Calculating the cell cycle scores
    def CellCycleScoring(self, path, adata, verbose=True):
    
        cell_cycle_genes = [x.strip() for x in open(path)]

        s_genes   = cell_cycle_genes[:43]
        g2m_genes = cell_cycle_genes[43:]


        if verbose:
            print(" * Searching s genes")

        s_genesIDs = []
        for gene in s_genes:
            A = self.__annotationMatrix.id[self.__annotationMatrix["symbol"] == gene]
            if len(A.values) > 0:
                for i in range(0, len(A.values)):
                    s_genesIDs.append(A.values[i])

        if verbose:
            print(" * Searching g2m genes")

        g2m_genesIDs = []
        for gene in g2m_genes:
            A = self.__annotationMatrix.id[self.__annotationMatrix["symbol"] == gene]
            if len(A.values) > 0:
                for i in range(0, len(A.values)):
                    g2m_genesIDs.append(A.values[i])  

        sc.tl.score_genes_cell_cycle(adata, s_genes=s_genesIDs, g2m_genes=g2m_genesIDs)


        if verbose:
            print(" * Cell cycle phase calculated")
    

    # Computing UMAP using the umap-learn package
    def computeUMAP(self, dataset1=None, dataset2=None, n_neighbors=5, fitAll=False):

        ump  = umap.UMAP(n_neighbors=n_neighbors, random_state=42)

        if (dataset1 is not None) and (dataset2 is not None):

            if fitAll:
                print(" * Fitting on all datasets")
                allData = np.append(dataset1, dataset2, axis=0)
                ump.fit(allData)
            else:
                print(" * Fitting only on dataset1")
                ump.fit(dataset1)

            return ump.transform(dataset1), ump.transform(dataset2)

        elif dataset1 is not None:
            print(" * Only dataset1")
            ump.fit(dataset1)
            return ump.transform(dataset1)

        elif dataset2 is not None:
            print(" * Only dataset2")
            ump.fit(dataset2)
            return ump.transform(dataset2)
    

    # Merging clusters using a user defined dictionary
    def mergeClusters(self, adata, newName=None, labels={}, cluster=None, verbose=False):

        toExit = False

        # Checking if the cluster is not None
        if cluster is None:
            print(" * Warning! None for cluster is an invalide column of .obs, please provide one of the column in .obs")
            toExit = True

        # Checking if the desidered group is in the .obs structure
        if cluster not in adata.obs.columns.tolist():
            print(" * Error! %s for cluster is an invalide column of .obs, please provide one of the column in .obs"%cluster)
            toExit = True
        
        if toExit:
            return
        
        if newName is None:
            newName = cluster + "_merged"
        
        if verbose:
            print(" * Merging clusters and renaming (column %s) ... "%newName)
         
        adata.obs[newName] = adata.obs[cluster]

        for label in labels:
        
            old  = adata.obs[newName][adata.obs[cluster] == label]
            new  = str(labels[label])
            
            if verbose:
                print(" \t* Old label %s - new label %s"%(label, new))

            adata.obs[newName].replace(old, new, inplace=True)

        adata.obs[newName] = adata.obs[newName].astype('category')
        
        if verbose:
            print(" *  Clusters merged")
    

    # Looking at the expression of genes provided by the user cluster by cluster
    def checkMarkerGenesPerCluster(self, adata, marker_cluster, clusterName=None, group_by=None, sortMarkerGenes=True):
        
        toExit = False

        # Checking if the group_by is not None
        if group_by is None:
            print(" * Warning! None for group_by is an invalide column of .obs, please provide one of the column in .obs")
            toExit = True

        # Checking if the desidered group is in the .obs structure
        if group_by not in adata.obs.columns.tolist():
            print(" * Error! %s for group_by is an invalide column of .obs, please provide one of the column in .obs"%group_by)
            toExit = True
        
        if toExit:
            return
    
        clusters = adata.obs[group_by].cat.categories    
        
        matrix = np.zeros((len(marker_cluster), len(adata.obs[group_by].cat.categories)))
            
        for row, gene in enumerate(marker_cluster):           
            for col, name in enumerate(clusters):
                cl            = adata[adata.obs[group_by] == name]
                geneExp_cl    = cl.raw.X[:, cl.var.index == gene]
                percentage_cl = 100*(len(geneExp_cl[geneExp_cl > 0])/cl.n_obs)
                
                matrix[row][col] = percentage_cl
        
        genes = pd.DataFrame(data=matrix, index=marker_cluster,  columns=clusters)
        
        if sortMarkerGenes:
            if clusterName is not None:
                genes = genes.sort_values(by=[clusterName], ascending=False)
            
        for gene in genes.index:
            print("* Gene %s is expressed in:"%('\033[1m'+gene+'\033[0m'))
            
            g = genes[genes.index == gene]
            g = g.sort_values(by=gene, ascending=False, axis=1)
            
            for c in g.columns:
                
                cl         = adata[adata.obs[group_by] == c]
                geneExp_cl = cl.raw.X[:, cl.var.index == gene]
                n_cells_cl = len(geneExp_cl[geneExp_cl > 0])            
                
                if c == clusterName:
                    print('\033[1m'+"\t -> %3.2f%% (%d out of %d) of cells of cluster %s"%(g[c], n_cells_cl, cl.n_obs, c)+'\033[0m')
                else:
                    print("\t -> %3.2f%% (%d out of %d) of cells of cluster %s"%(g[c], n_cells_cl, cl.n_obs, c))
            
            display(g)

    

# ****************************************** Metadata functions ******************************************
    # Loading the provided metadata
    def loadMetadata(self, path, adata, delimiter=",", verbose=True, rows=10):
        
        adataCopy = adata.copy()
        
        if verbose:
            print(" * Loading metadata, path=%s"%path)

        metadata = pd.read_csv(path, delimiter=delimiter) 
        metadata.rename(index=metadata.CELL_NAME, inplace=True)
        metadata = metadata.drop(columns=['Unnamed: 0'])

        adataCopy.obs = pd.concat([adata.obs, metadata.loc[adata.obs.index.tolist()]], axis=1)
        adataCopy.obs.sort_index(axis=1, inplace=True)

        if verbose:
            display(adataCopy.obs.head(rows))

        return adataCopy
    
    
    # Assigning the origin to cells
    def assignOrigin(self, adata, namesIn=[], namesOut=[], verbose=True):
    
        adataCopy = adata.copy()
        adataCopy.obs['origin'] = adataCopy.obs.COMMON_NAME

        for idx,name in enumerate(namesIn):
            names = adataCopy.obs.origin[adataCopy.obs.origin.str.contains(name)]
            adataCopy.obs['origin'].replace(names, namesOut[idx], inplace=True)
        adataCopy.obs['origin'] = adataCopy.obs.origin.astype('category')

        if verbose:
            print(adataCopy.obs.origin.cat.categories.tolist())

        return adataCopy

    
    # Assigning the gate to cells
    def assignGate(self, adata, namesIn=[], namesOut=[], verbose=True):

        adataCopy = adata.copy()
        adataCopy.obs['gate'] = adataCopy.obs.COMMON_NAME

        for idx,name in enumerate(namesIn):
            names = adataCopy.obs.gate[adataCopy.obs.gate.str.contains(name)]
            adataCopy.obs['gate'].replace(names, namesOut[idx], inplace=True)
        adataCopy.obs['gate'] = adataCopy.obs.gate.astype('category')

        if verbose:
            print(adataCopy.obs.gate.cat.categories.tolist())

        return adataCopy

    
    # Assigning the cell type to cells
    def assignCellType(self, adata, namesIn=[], namesOut=[], verbose=True):

        adataCopy = adata.copy()
        adataCopy.obs['cell_type'] = adataCopy.obs.COMMON_NAME

        for idx,name in enumerate(namesIn):
            names = adataCopy.obs.cell_type[adataCopy.obs.cell_type.str.contains(name)]
            adataCopy.obs['cell_type'].replace(names, namesOut[idx], inplace=True)
        adataCopy.obs['cell_type'] = adataCopy.obs.cell_type.astype('category')

        if verbose:
            print(adataCopy.obs.cell_type.cat.categories.tolist())

        return adataCopy



# ****************************************** Plotting functions using the same style ******************************************    
    # Plotting a group of violin plots based on a variable
    def plotViolinVariableGroup(self, adata, variable, group_by=None, log=False, cut=0, pointSize=4,
                                width=8, height=8, ax=None, use_raw=False, rotation=None):
        
        toExit = False

        # Checking if the group_by is not None
        if group_by is None:
            print(" * Warning! None for group_by is an invalide column of .obs, please provide one of the column in .obs")
            toExit = True

        # Checking if the desidered group is in the .obs structure
        if group_by not in adata.obs.columns.tolist():
            print(" * Error! %s for group_by is an invalide column of .obs, please provide one of the column in .obs"%group_by)
            toExit = True
        
        if toExit:
            return
        
        if ax is not None:
            sc.pl.violin(adata, variable, groupby=group_by, log=log, jitter=0.4, size=pointSize,
                         multi_panel=False, ax=ax, show=False, cut=0, use_raw=use_raw, rotation=rotation)
            
        else:
            
            f, axs = plt.subplots(1,1,figsize=(width,height))
            sns.set(font_scale=1.5)
            sns.set_style("white")
            
            sc.pl.violin(adata, variable, groupby=group_by, log=log, jitter=0.4, size=pointSize,
                         multi_panel=False, ax=axs, show=False, cut=0, use_raw=use_raw, rotation=rotation)

            sns.despine(offset=10, trim=False)
            plt.tight_layout()
            plt.show(block=False)
    

    # Plotting a scatter plot based on two variables (x and y)
    def plotScatter(self, adata, variable, x="n_genes", y="ercc_content", palette=["gray"],
                    pointSize=150, width=8, height=8, ax=None, colormap=None):
            
        if ax is not None:
            if colormap is None:
                sc.pl.scatter(adata, x=x, y=y, color=variable, palette=palette,
                              size=pointSize, ax=ax, show=False)
            else:
                sc.pl.scatter(adata, x=x, y=y, color=variable, color_map=colormap,
                              size=pointSize, ax=ax, show=False)
                
        
        else:
            
            plt.rcParams['figure.figsize']=(10,10)
            sns.set(font_scale=1.5)
            sns.set_style("white")
            
            if colormap is None:
                sc.pl.scatter(adata, x=x, y=y, color=variable, palette=palette,
                              size=pointSize, show=False)
            else:
                sc.pl.scatter(adata, x=x, y=y, color=variable, color_map=colormap,
                              size=pointSize, show=False)
            
            sns.despine(offset=10, trim=False)
            plt.show(block=False)
    
    
    # Plotting a histogram plot based on a variable
    def plotHistrogram(self, variable, kde=False, bins=50, color="black", width=8, height=8, ax=None):
        
        if ax is not None:
            
            sns.distplot(variable, bins=bins, kde=kde, color=color)
                
        else:
            
            f, axs = plt.subplots(1,1,figsize=(width,height))
            sns.set(font_scale=1.5)
            sns.set_style("white")
            
            sns.distplot(variable, bins=bins, kde=kde, ax=axs, color=color)
            
            sns.despine(offset=10, trim=False)
            plt.show(block=False)


    # Plotting a scatter plot for each cluster with the name of the top n marker genes
    def plotGenesRankGroups(self, adata, n_genes=20, key="rank_genes_groups", ncols=2, fontsize=12):

        sns.set(font_scale=1.5)
        sns.set_style("white")

        sc.pl.rank_genes_groups(adata,
                                n_genes=n_genes,
                                ncols=ncols,
                                key=key,
                                fontsize=fontsize,
                                show=False)

        sns.despine(trim=False)

    
    # Plotting two UMAP components colouring the cells based on a variable 
    def plotUMAP(self, adata, variable=None, pointSize=150, width=8, height=8, palette=None,
                 components='1,2', ax=None, colormap=None, use_raw=False):

        toExit = False

        # Checking if the variable is not None
        if variable is None:
            print(" * Warning! None for variable is an invalide column of .obs, please provide one of the column in .obs")
            toExit = True
        
        if toExit:
            return
        
        # Checking and palette to be used
        if palette is None:

            try:
                palette = adata.uns[variable+"_colors"]
            except:
                palette = sns.color_palette("deep")
        else:
            if isinstance(palette[0], tuple):
                palette = self.__rgb2hex(palette)
        
        # Using the provided axis
        if ax is not None:

            if variable is None:
                sc.pl.umap(adata, size=pointSize, palette=palette, components=components, ax=ax, show=False, use_raw=use_raw)

            else:
                if colormap is None:
                    sc.pl.umap(adata, color=variable, size=pointSize, palette=palette, components=components, ax=ax, show=False, use_raw=use_raw)
                else:
                    sc.pl.umap(adata, color=variable, size=pointSize, cmap=colormap, components=components, ax=ax, show=False, use_raw=use_raw)
        
        # Create a figure to plot the UMAP     
        else:      
            f, axs = plt.subplots(1,1,figsize=(width,height))
            sns.set(font_scale=1.5)
            sns.set_style("white")

            if variable is None:
                sc.pl.umap(adata, size=pointSize, palette=palette, components=components, ax=axs, show=False, use_raw=use_raw)

            else:
                if colormap is None:
                    sc.pl.umap(adata, color=variable, size=pointSize, palette=palette, components=components, ax=axs, show=False, use_raw=use_raw)
                else:
                    sc.pl.umap(adata, color=variable, size=pointSize, cmap=colormap, components=components, ax=axs, show=False, use_raw=use_raw)

            sns.despine(offset=10, trim=False)
            plt.tight_layout()
            plt.show(block=False)
    

    # Plotting the data in a 3D space using plotly library. The components must be in the .obsm structure.
    def plot3D(self, adata, group_by=None, space=None, title="",
           poinSize=6, showgrid=True, palette=None):
        
        toExit = False

        # Checking if the desidered space is not None
        if space is None:
            print(" * Warning! None for space is an invalide space, please provide one of the space in .obsm")
            toExit = True
        
        # Checking if the group_by is not None
        if group_by is None:
            print(" * Warning! None for group_by is an invalide column of .obs, please provide one of the column in .obs")
            toExit = True

        # Checking if the desidered group is in the .obs structure
        if group_by not in adata.obs.columns.tolist():
            print(" * Error! %s for group_by is an invalide column of .obs, please provide one of the column in .obs"%group_by)
            toExit = True
        
        # Checking if the desidered space is in the .obsm structure
        if space not in list(adata.obsm):
            print(" * Error! %s space is not available, please calculate the %s space and save it in .obsm['%s']"%(space, space, space))
            print(" * The available are:", list(adata.obsm))
            toExit = True
        
        if toExit:
            return
        
        try:
            ax_label = space.split("_")[1]
        except:
            ax_label = space
        
        # Checking and palette to be used
        if palette is None:

            try:
                palette = adata.uns[group_by+"_colors"]
            except:
                palette = sns.color_palette("deep")
        else:
            if isinstance(palette[0], tuple):
                palette = self.__rgb2hex(palette)
        
        keys  = adata.obs[group_by].cat.categories.tolist()
        try:
            keys = sorted(map(int, keys))
        except:
            pass
        
        listGroups = []

        # Retrieve the first 3 components
        for idx,key in enumerate(keys):

            data  = adata[adata.obs[group_by] == str(key)]
            data  = data.obsm[space]

            x = data[:,0]
            y = data[:,1]
            z = data[:,2]

            trace = go.Scatter3d(x=x,
                                 y=y,
                                 z=z,
                                 mode='markers',
                                 marker=dict(size=poinSize, color=palette[idx%len(palette)]),
                                 name=str(key)
                    )

            listGroups.append(trace)
        
        # Setting plotly layout
        layout = go.Layout(scene = dict(xaxis = dict(title=ax_label.upper() + " 1",
                                                     showbackground=False,
                                                     showgrid=showgrid,
                                                     gridcolor="rgb(218,218,218)",
                                                     zeroline=False,
                                                     showline=True,
                                                     ticks='',
                                                     showticklabels=False
                                                    ),
                                        yaxis = dict(title=ax_label.upper() + " 2",
                                                     showbackground=False,
                                                     gridcolor="rgb(218,218,218)",
                                                     showgrid=showgrid,
                                                     zeroline=False,
                                                     showline=True,
                                                     ticks='',
                                                     showticklabels=False
                                                    ),
                                        zaxis = dict(title=ax_label.upper() + " 3",
                                                     showbackground=False,
                                                     gridcolor="rgb(218,218,218)",
                                                     showgrid=showgrid,
                                                     zeroline=False,
                                                     showline=True,
                                                     ticks='',
                                                     showticklabels=False
                                                    )
                                       ),
                           height=600,
                           legend=dict(orientation="h", y=.05),
                           margin=dict(r=0, b=0, l=0, t=0)
                          )
        fig = go.Figure(data=listGroups, layout=layout)
        py.offline.iplot(fig, filename=title)
        

    # Plotting pie-charts using plotly
    def plotPieCharts(self, adata, variable=None, group_by=None, cols=2, palette=None):

        toExit = False

        # Checking if the group_by is not None
        if group_by is None:
            print(" * Warning! None for group_by is an invalide column of .obs, please provide one of the column in .obs")
            toExit = True
        
        # Checking if the variable is not None
        if variable is None:
            print(" * Warning! None for variable is an invalide column of .obs, please provide one of the column in .obs")
            toExit = True

        # Checking if the desidered group is in the .obs structure
        if group_by not in adata.obs.columns.tolist():
            print(" * Error! %s for group_by is an invalide column of .obs, please provide one of the column in .obs"%group_by)
            toExit = True
        
        # Checking if the desidered variable is in the .obs structure
        if variable not in adata.obs.columns.tolist():
            print(" * Error! %s for variable is an invalide column of .obs, please provide one of the column in .obs"%variable)
            toExit = True
        
        if toExit:
            return
    
        keys = adata.obs[group_by].unique().tolist()

        try:
            keys = sorted(map(int, keys))
        except:
            keys = sorted(keys)

        # Checking and palette to be used
        if palette is None:
                      
            try:
                palette = adata.uns[variable+"_colors"]
            except:
                palette = None
        else:
            if isinstance(palette[0], tuple):
                palette = self.__rgb2hex(palette)

        if len(keys) > 1:
            rows = int(len(keys)/cols)

            if rows*cols < len(keys):
                rows += 1

        else:
            rows = 1
            cols = 1

        # Create subplots: use 'domain' type for Pie subplot
        fig = make_subplots(rows=rows,
                            cols=cols,
                            specs=[[{'type':'domain'}]*cols]*rows,
                            subplot_titles=keys)
        
        variables = sorted(adata.obs[variable].unique().tolist())
        for r in range(0, rows):
            for c in range(0, cols):
                idx = r*cols + c
                if idx >= len(keys):
                    break

                gp = adata[adata.obs[group_by] == str(keys[idx])]
                df = gp.obs.groupby([variable]).size()
                df = pd.DataFrame({variable:df.index, 'Counts':df.values})
                if palette is None:
                    fig.add_trace(go.Pie(labels=df[variable],
                                         values=df["Counts"]),
                                  r+1, c+1)
                else:
                    colors = []
                    
                    for v in df[variable]:
                        for i,v1 in enumerate(variables):
                            if v == v1:
                                colors.append(palette[i%len(palette)])
                    
                    fig.add_trace(go.Pie(labels=df[variable],
                                         values=df["Counts"],
                                         marker_colors=colors),
                                  r+1, c+1)
                    

        # Use `hole` to create a donut-like pie chart
        fig.update_traces(hole=.4, hoverinfo="label+value")
        
        if 500*cols > 1000:
            width = 1000
        else:
            width = 500*cols
        
        offset = 0.01
        if len(np.unique(adata.obs[variable])) > 6:
            offset = len(np.unique(adata.obs[variable]))/1000.

        fig.update_layout(
            height=rows*600, width=width,
            title_text=variable,
            legend=dict(orientation="h", x=0.2, y=1.0+offset))

        fig.show()
    

    # Plotting the HVGs
    def plotHVGs(self, adata, width=8, height=8):
    
        sns.set(rc={'figure.figsize':(width,height)})
        sns.set(font_scale=1.5)
        sns.set_style("white")

        sc.pl.highly_variable_genes(adata, show=False)

        sns.despine(offset=10, trim=False)
        plt.show(block=False)
    

    # Plotting a scatter plot with two series
    def plotScatterDatasets(self, dataset1=None, dataset2=None, axis=None, label1="Real", label2="Synthetic", title="", width=8, height=8):

        if axis is None:
            f, axis = plt.subplots(1,1,figsize=(width,height))
            sns.set(font_scale=1.5)
            sns.set_style("white")

        if dataset1 is not None:
            axis.scatter(dataset1[:,0], dataset1[:,1], s=36, label=label1)

        if dataset2 is not None:
            axis.scatter(dataset2[:,0], dataset2[:,1], s=12, label=label2)

        axis.legend()
        axis.set_title(title)

        if axis is None:
            sns.despine(offset=10, trim=False)
            plt.tight_layout()
            plt.show()


    # Plotting ridge plots grouping the data
    def ridge_plot(self, adata, genes=[], group_by=None, use_raw=True, height=0.75, palette=None):

        toExit = False
        
        # Checking if the group_by is not None
        if group_by is None:
            print(" * Warning! None for group_by is an invalide column of .obs, please provide one of the column in .obs")
            toExit = True

        # Checking if the desidered group is in the .obs structure
        if group_by not in adata.obs.columns.tolist():
            print(" * Error! %s for group_by is an invalide column of .obs, please provide one of the column in .obs"%group_by)
            toExit = True
        
        if toExit:
            return

        if palette is None:

            try:
                palette = adata.uns[group_by+"_colors"]
            except:
                palette = sns.color_palette("deep")
        else:
            if isinstance(palette[0], tuple):
                palette = self.__rgb2hex(palette)
    
        if not isinstance(genes, list):
            genes = [genes]
        
        for gene in genes:
        
            if use_raw:
                genes_exp = adata.raw.X[:, adata.var.index==gene]
            else:
                genes_exp = adata.X[:, adata.var.index==gene]
                            
            dataframe = pd.DataFrame(data=genes_exp, index=adata.obs.index, columns=[gene])
            dataframe[group_by] = adata.obs[group_by]  
            self.__plotSingleRidgePlot(dataframe, palette, height=height)
