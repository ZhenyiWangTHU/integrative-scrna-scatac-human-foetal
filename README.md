# Integrative scRNA-scATAC human foetal

This repository contains all the scripts and pipelines used to process and analyse single-cell cell RNA sequencing (scRNA-Seq) and single-cell Assay for Transposase Accessible Chromatin sequencing (scATAC-Seq) data.

The main pipeline underlying the analysis of scRNA-Seq data is the one proposed in Tangherloni _et al._: _scAEspy: a unifying tool based on autoencoders for the analysis of single-cell RNA sequencing data_, <a href="https://www.biorxiv.org/content/10.1101/727867v1">biorxiv</a>, 727867, 2019. doi: 10.1101/727867.
The code was developed for the study **Integrative single-cell RNA-Seq and ATAC-Seq analysis of human foetal liver and bone marrow haematopoiesis** but we are currently using it in other projects.

All the analyses of scRNA-Seq data can be easily run by downloading the data as explained in ([Data](#data)) and using the provided <a href="https://gitlab.com/cvejic-group/integrative-scrna-scatac-human-foetal/-/blob/master/Functions/scRNA_functions.py">functions</a> and <a href="https://gitlab.com/cvejic-group/integrative-scrna-scatac-human-foetal/-/tree/master/RNA_analyses">Jupyter Notebooks</a>.

  1. [References](#ref) 
  2. [Required libraries](#lib) 
  3. [Data](#data)
  4. [Metadata](#metadata)
  5. [Jupyter Notebooks](#notebooks)
  6. [License](#lic)
  7. [Contacts](#cont)

## <a name="ref"></a>References ##

A detailed description of the conducted study and achieved results can be found in:

- Ranzoni A.M., Tangherloni A., Berest I., Riva S.G., Myers B., Strzelecka P. Xu J., Panada E., Mohorianu I., Zaugg J., and Cvejic A.: _Integrative single-cell RNA-Seq and ATAC-Seq analysis of human foetal liver and bone marrow haematopoiesis_, <a href="https://www.biorxiv.org/content/10.1101/2020.05.06.080259v1">biorxiv</a>, 2020.05.06.080259, 2020. doi: 10.1101/2020.05.06.080259.

Ranzoni and Tangherloni equally contributed to this project.

## <a name="lib"></a>Required libraries ##

All the code used to analyse the scRNA-Seq data has been developed in `Python (v.3.6.9)` and tested on Ubuntu Linux and MacOS X machines.
To be able to run all the analyses shown in [Jupyter Notebooks](#notebooks), the following packages are required:
- `scanpy (v.1.4.5.1)`
- `pandas (v.0.25.3)`
- `umap-learn (v.0.3.10)`
- `leidenalg (v.0.7.0)`
- `python-igraph (v.0.7.1.post6)`
- `bbknn (v.1.3.6)`
- `gprofiler (v.1.2.2)`
- `gseapy (v.0.9.16)`
- `numpy (v.1.18.2)`
- `scipy (v.1.4.1)`
- `scikit-learn (v.0.21.2)`
- `keras (v.2.2.4)`
- `tensorflow (v.1.12.0)`
- `matplotlib (v.3.1.3)`
- `seaborn (v.0.9.0)`
- `plotly (v.4.5.3)`
- `fa2 (v.0.3.5)`

The code used for the upstream analysis of the scATAC-Seq data has been developed using the following tools:
- `samtools (v.1.9)`
- `bedtools (v.2.27.1)`
- `Picard (v.2.9.0)`
- `BWA (v.0.7.17)`
- `SnapATAC`

The code used for the downstream analysis of the scATAC-Seq data has been developed in `R (v.3.6.1)` and tested on Ubuntu Linux machines.
To be able to run all the analyses the following packages are required:
- `Seurat (v.3.1.4)`
- `Signac (v.0.2.4)`
- `chromVAR (v.1.8)`
- `Harmony (v.1.0)`

## <a name="data"></a>Data ##

The folder <a href="https://gitlab.com/cvejic-group/integrative-scrna-scatac-human-foetal/-/tree/master/Data">Data</a> includes the following subfolders:
- <a href="https://gitlab.com/cvejic-group/integrative-scrna-scatac-human-foetal/-/tree/master/Data/Samples">Samples</a> contains the gene expression matrices of our 15 scRNA-Seq samples as well as the fragment matrices of our 3 scATAC-Seq samples;
- <a href="https://gitlab.com/cvejic-group/integrative-scrna-scatac-human-foetal/-/tree/master/Data/ScanpyObjets">ScanpyObjets</a> contains the `SCANPY`'s objects generated and used in the [Jupyter Notebooks](#notebooks);
- <a href="https://gitlab.com/cvejic-group/integrative-scrna-scatac-human-foetal/-/tree/master/Data/scATAC_CSV_file_for_Scanpy">scATAC_CSV_file_for_Scanpy</a> contains the `.CSV` files necessary to build the `SCANPY`'s object used to infer the differentiation trajectories of our 3 scATAC-Seq samples.

The raw RNA-Seq data (i.e., fastq files) can be downloaded from ArrayExpress (accession code X-XXXX-0000), while the raw ATAC-Seq data (i.e., fastq files) can be downloaded from ArrayExpress (accession code Y-YYYY-0000).

## <a name="metadata"></a>Metadata ##

The <a href="https://gitlab.com/cvejic-group/integrative-scrna-scatac-human-foetal/-/tree/master/MetaData">MetaData</a> folder is comprised of the following subfolders:
- <a href="https://gitlab.com/cvejic-group/integrative-scrna-scatac-human-foetal/-/tree/master/MetaData/Resources">Resources</a> contains the `.CSV` files generated during the analyses, the annotation matrix where there is a mapping between Ensembl IDs and gene names, the excel file with the manually curated marker genes, and the genes used to compuate the cell cycle state of each cell. The <a href="https://gitlab.com/cvejic-group/integrative-scrna-scatac-human-foetal/-/tree/master/MetaData/Resources/Subsampling">Subsampling</a> folder contains the results obtained by our subsampling strategy;
- <a href="https://gitlab.com/cvejic-group/integrative-scrna-scatac-human-foetal/-/tree/master/MetaData/Samples">Samples</a> contains the metadata information (e.g., organ, gate, age) of our scRNA-Seq and scATAC-Seq samples.

## <a name="notebooks"></a>Jupyter Notebooks ##

The folder <a href="https://gitlab.com/cvejic-group/integrative-scrna-scatac-human-foetal/-/tree/master/RNA_analyses">RNA_analyses</a> contains the Jupyter Notebooks developed to analyse our scRNA-Seq data. Specifically, the notebook <a href="https://gitlab.com/cvejic-group/integrative-scrna-scatac-human-foetal/-/blob/master/RNA_analyses/1_QualityControl.ipynb">1_QualityControl</a> contains all the QC steps that we did (i.e., filtering of the number of cells based on the number of counts and expressed genes as well as the mitochondrial content). The 15 samples have been merged into a single `Scanpy`'s object and the cell cycle state for each cell calculated. In the notebook <a href="https://gitlab.com/cvejic-group/integrative-scrna-scatac-human-foetal/-/blob/master/RNA_analyses/2a_DownstreamAnalysis-Autoencoder.ipynb">2a_DownstreamAnalysis-Autoencoder</a>, we used Autoencoders as dimensionality reduction approach to capture the non-linearities among the gene interactions that may exist in the high-dimensional expression space of scRNA-Seq data. In the notebook <a href="https://gitlab.com/cvejic-group/integrative-scrna-scatac-human-foetal/-/blob/master/RNA_analyses/2b_DownstreamAnalysis-BBKNN-Clustering.ipynb">2b_DownstreamAnalysis-BBKNN-Clustering</a>, we applied the batch balanced *k* nearest neighbours (BBKNN) approach to remove the batch effects among the 15 samples and Leiden algorithm to cluster the neighbourhood graph. The notebook <a href="https://gitlab.com/cvejic-group/integrative-scrna-scatac-human-foetal/-/blob/master/RNA_analyses/2c_DownstreamAnalysis-ManualAnnotation.ipynb">2c_DownstreamAnalysis-ManualAnnotation</a> contains the manual annotation of the clusters found in the previous notebook. In the notebook <a href="https://gitlab.com/cvejic-group/integrative-scrna-scatac-human-foetal/-/blob/master/RNA_analyses/3_TrajectoryAnalysis.ipynb">3_TrajectoryAnalysis</a>, we applied the ForceAtlas2 (FA2) drawing algorithm and Partition-based Approximate Graph Abstraction (PAGA) to infer the differentiation trajectory of our scRNA-Seq data. The notebook <a href="https://gitlab.com/cvejic-group/integrative-scrna-scatac-human-foetal/-/blob/master/RNA_analyses/4_MetabolicGenes.ipynb">4_MetabolicGenes</a> contains the analysis of the genes related to metabolism. The notebook <a href="https://gitlab.com/cvejic-group/integrative-scrna-scatac-human-foetal/-/blob/master/RNA_analyses/5_Classification_MarkerGenes.ipynb">5_Classification_MarkerGenes</a> contains the cell type classification by using a Random Forest classifier and the proposed Deep Neural Network to validate our cell type annotation. The notebooks <a href="https://gitlab.com/cvejic-group/integrative-scrna-scatac-human-foetal/-/blob/master/RNA_analyses/6a_StatisticalAnalysis-NumberCells.ipynb">6a_StatisticalAnalysis-NumberCells</a>, <a href="https://gitlab.com/cvejic-group/integrative-scrna-scatac-human-foetal/-/blob/master/RNA_analyses/6b_StatisticalAnalysis-NumberGenes.ipynb">6b_StatisticalAnalysis-NumberGenes</a>, and <a href="https://gitlab.com/cvejic-group/integrative-scrna-scatac-human-foetal/-/blob/master/RNA_analyses/6c_StatisticalAnalysis-CellCycle.ipynb">6c_StatisticalAnalysis-CellCycle</a> contain the statistical analysis about the number of cells, number of expressed genes, and cell cycle state, respectively. Finally, the notebooks <a href="https://gitlab.com/cvejic-group/integrative-scrna-scatac-human-foetal/-/blob/master/RNA_analyses/7_SubsamplingStrategy.ipynb">7_SubsamplingStrategy</a> and <a href="https://gitlab.com/cvejic-group/integrative-scrna-scatac-human-foetal/-/blob/master/RNA_analyses/8_DifferentialExpressionAnalysis.ipynb">8_DifferentialExpressionAnalysis</a> contain the designed subsampling strategy and the  differential expression analysis, respectively.

## <a name="lic"></a>License ##

The code and metadata are licensed under the terms of the GNU GPL v3.0

## <a name="cont"></a>Contacts ##

For questions or support, please contact Dr. Andrea Tangherloni (<at860@cam.ac.uk>), Simone Giovanni Riva (<sgr34@cam.ac.uk>), Ivan Berest (<ivan.berest@embl.de>), and Irina Mohorianu(<iim22@cam.ac.uk>).
